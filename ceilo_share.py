#!/usr/bin/env python3

import datetime
import numpy as np
import ftplib
import shutil
import os
import glob
import re
import subprocess
import yaml
import netCDF4 as nc
from netCDF4 import Dataset, num2date
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.colors import LogNorm, Normalize
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

with open('/usr/people/zwagers/lidar/config.yml', 'r') as ymlfile:
    cfg = yaml.safe_load(ymlfile)

# I/O vars
sensordata = cfg['sensordata']
datadir = cfg['datadir']
pngdir = cfg['pngdir']
archive = cfg['archive']
longterm = cfg['longterm']
prefix = cfg['prefix']
postfix = cfg['postfix']
lastrun = cfg['lastrun']

# global vars
number_of_hours = cfg['number_of_hours']
number_of_hours_archive = cfg['number_of_hours_archive']
convert_to_feet = 0.3048
stations = cfg['stations']

#plot vars
myFmt = mdates.DateFormatter('%H')
datumtijd = ''

# in: timestamp = YYYYMMDDHHII
# in: hours = integer, denoting how many hours back
# in: station id
# out: array of files, size = number_of_hours * interval_in_minutes
def timespan(timestamp, hours, station):
    out = []
    YYYY = int(timestamp[0:4])
    MM = int(timestamp[4:6])
    DD = int(timestamp[6:8])
    HH = int(timestamp[8:10])
    II = int(timestamp[10:12]) 
    start = datetime.datetime(YYYY, MM, DD, HH, II)
    number_of_iterations = int(hours*60)
    for i in range(0,number_of_iterations+1):
        previous = start - datetime.timedelta(minutes=i)
        out.append(station+prefix+previous.strftime("%Y%m%d%H%M")+postfix)
    
    return out
    
def verschil_in_uren(t1, t2):
    YYYY = int(t1[0:4])
    MM = int(t1[4:6])
    DD = int(t1[6:8])
    HH = int(t1[8:10])
    II = int(t1[10:12]) 
    start = datetime.datetime(YYYY, MM, DD, HH, II)
    YYYY = int(t2[0:4])
    MM = int(t2[4:6])
    DD = int(t2[6:8])
    HH = int(t2[8:10])
    II = int(t2[10:12]) 
    end = datetime.datetime(YYYY, MM, DD, HH, II)
    
    return end-start
    
def retrieve_data(station, wid):
    year = str(datetime.datetime.now().year)
    month = '{:02d}'.format(datetime.datetime.now().month)
    day = '{:02d}'.format(datetime.datetime.now().day)
    
    # welke bestanden zitten er in de directory voor dit station
    folder = sensordata+year+'/'+month+'/'+day+'/'
    filelist = glob.glob(folder+station+'*'+postfix)
    filelist.sort() 
    
    # newest file
    try:
        newest_file = filelist[-1]
    except:
        return False
    
    # timestamp of newest file
    timestamp = re.findall(r'\d{12}', newest_file)
    datumtijd = timestamp[0]
    
    # which files do we need at this time:
    needed_files = timespan(timestamp[0], number_of_hours, station)
    
    # remove files we do not need anymore
    list_of_files = os.listdir(datadir+station)
    for f in list_of_files:
        if f not in needed_files:
            os.remove(datadir+station+'/'+f)
            pass
    
    # collect files which are not there yet:
    files_to_retrieve = []
    for f in needed_files:
        if os.path.isfile(datadir+station+'/'+f)==False:
            files_to_retrieve.append(f)
    
    # If nothing to retrieve, then no new data. Return and close connection
    if len(files_to_retrieve)==0:
        return False
        
    # retrieve collected files  
    for f in files_to_retrieve:
        try:
            tijd = re.findall("[0-9]{12}", f)
            year = tijd[0][0:4]
            month = tijd[0][4:6]
            day = tijd[0][6:8]
            folder = sensordata+year+'/'+month+'/'+day+'/'
            shutil.copy(folder+f,datadir+station[-7:]+'/'+f) 
            
        except:
            pass
            
    # concatenate all files into one (station_id.nc)
    list_of_files = glob.glob(datadir+station+'/*.nc')
    list_of_files.sort()
    
    for f in list_of_files:
        statinfo = os.stat(f)
        # size of file is 0, don´t include
        if statinfo.st_size==0:
            os.remove(f)
        # if Test file, don´t include
        else:    
            fh = Dataset(f, mode='r')
            if fh.getncattr('device_name')[2] == 'T':
                os.remove(f)

    list_of_files = glob.glob(datadir+station+'/*.nc')
    list_of_files.sort()
            
    ncfile = datadir+station+'/'+station+'.nc'
    subprocess.call(['/usr/bin/ncrcat'] + list_of_files + ['-O', ncfile])
    
    # remove oldest png from archive if more than 24-hours old
    list_of_files = glob.glob(archive+'lidar'+station+'_*.png')
    list_of_files.sort()
    for f in list_of_files:
        timestamp = re.findall(r'\d{12}', f)
        delta = verschil_in_uren(timestamp[0], datumtijd)
        #print (f, timestamp[0], delta, delta.seconds/3600 + delta.days*24)
        if delta.seconds/3600 + delta.days*24>24:
            os.remove(f)
    
    return datumtijd
    
# plot data and save as png
def create_plot(station, datumtijd, last, uur, minuut):
    
    # read big netcdf-file, containing last number_of_hours
    ncfile = datadir+station+'/'+station+'.nc'
    print (ncfile)
    fh = Dataset(ncfile, mode='r')
    height = fh.variables['range'][:]
    time = num2date(fh.variables['time'][:],units=fh.variables['time'].units,calendar='standard', only_use_cftime_datetimes=False)
    sig = fh.variables['beta_raw'][:,:]     # raw signal
    cbh = fh.variables['cbh'][:,:]          # cloud base height
    height /= convert_to_feet               # meters to feet
    location = fh.getncattr('comment')      # volledige naam station
        
    # upper plot
    fig=plt.figure(num='Base',dpi=100,figsize=(29/2.54,21.7/2.54),frameon=True)
    frame=fig.add_subplot(211, facecolor='k')
    plt.title('Lufft CHM15k ('+station+' '+location+')\n last update: '+datumtijd)
    plt.xlabel('Time (UTC)')
    plt.ylabel('Height (ft)')    
    plt.pcolormesh(time,height,sig.T,cmap=plt.cm.nipy_spectral,norm=LogNorm(vmin=3e+4, vmax=3e+8, clip=True), shading='auto')
    cbhmask = np.nonzero(cbh[:,1])
    plt.plot(time[cbhmask],cbh[cbhmask]/convert_to_feet,'w',marker='s',linestyle='None',markersize=2,mec='k')
    frame.xaxis.set_major_formatter(myFmt)
    frame.set_ylim([height.min(),40000])
    
    # grid upper plot
    plt.grid(color='w')
    
    # lower plot
    frame=fig.add_subplot(212, facecolor='k')
    plt.pcolormesh(time,height,sig.T,cmap=plt.cm.nipy_spectral, norm=LogNorm(vmin=3e+4, vmax=3e+8, clip=True), shading='auto')
    plt.ylabel('Height (ft)')
    frame.xaxis.set_major_formatter(myFmt)
    frame.set_ylim([height.min(),5000])
    
    # plot cloud base heights as black marker
    plt.plot(time[cbhmask],cbh[cbhmask]/convert_to_feet,'w',marker='s',linestyle='None',markersize=3,mec='k')
    
    # grid lower plot
    plt.grid(color='w')
    
    # colorbar
    cax = fig.add_axes([0.0825, -0.025, 0.90, 0.02])
    plt.colorbar(orientation='horizontal',label='Normalized Range Corrected Signal (1)',extend='both',cax=cax)
    
    # save image
    output = pngdir+'lidar'+station+'.png'
    print (output)
    plt.savefig(output, bbox_inches='tight')
    
    # save image also to archive
    output = archive+'lidar'+station+'_'+last+'.png'
    plt.savefig(output, bbox_inches='tight')
    
    # if 00/06/12/18, save to long term archive
    if int(uur) in [0,6,12,18] and int(minuut) < 10:
        output = longterm+'lidar'+station+'_'+last+'.png'
        plt.savefig(output, bbox_inches='tight')
    
    # close plot
    plt.close()
    
    # save last run in pngdir
    list_of_files = glob.glob(archive+'lidar'+station+'_*.png')
    list_of_files.sort(reverse=True)
    f = open(pngdir+station+lastrun, 'w')
    for i in list_of_files:
        f.write(i.replace(archive, '')+'\n')
    f.close()
    
    # save last run in pngdir
    f = open(pngdir+lastrun, 'w')
    f.write(last)
    f.close()
    
# start, plot all data, if new
for s in stations:
    synop = re.findall("[0-9]{5}_[A-Z]", s)
    wigos = s.replace(synop[0], '')
    plot = retrieve_data(synop[0], wigos)
    if (plot):
        YYYY = int(plot[0:4])
        MM = int(plot[4:6])
        DD = int(plot[6:8])
        HH = int(plot[8:10])
        II = int(plot[10:12]) 
        datumtijd = datetime.datetime(YYYY, MM, DD, HH, II)
        try:
            create_plot(synop[0], datumtijd.strftime("%d-%m-%Y %H:%M UTC"), plot, HH, II)
        except FileNotFoundError:
            print ("File not found!")
