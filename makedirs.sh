#!/usr/bin/bash

directory="/nobackup/users/zwagers/lidar/"

declare -a stations=(
06260_A 
06261_A 
06348_A 
06280_A 
06310_A 
06215_A 
06380_A 
06240_B 
06235_A 
06344_A 
06240_C 
06267_A 
06275_A 
06290_A 
06370_A 
06233_A 
06375_A 
06350_A 
06237_A 
78990_A 
06238_A 
06261_B 
06270_A 
06269_A 
06377_A 
06279_A 
06242_A 
06236_A 
06340_A 
06317_A 
06240_D 
)

# if directory does not exist, create it

if [ ! -d $directory ]; then
	echo "directory $directory does not exist, create it\n"
	mkdir $directory
fi

for i in "${stations[@]}"
do
	if [ ! -d $directory$i ]; then
		echo "$directory$i does not exist, create it"
		mkdir $directory$i
	fi
done
