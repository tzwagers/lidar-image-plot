# Lidar Image Plot

Python script to read read lidar data (NetCDF) and plot the data as png.

![](<example images/lidar06260_A_202202090815.png>)
